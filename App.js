/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  NativeModules,
  TouchableOpacity,
  Alert,
  Platform
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export default class App extends Component {
  constructor(props) {
    super(props)
  }

  calculateSum() {
    if (Platform.OS === "ios") {
      NativeModules.Bulb.getSum( (error, sum) => {
        alert(sum)
      })
    } else {
      NativeModules.Native.sayHi( 
        (err) => {
          console.log(err)
        }, 
        (msg) => {
          console.log(msg) 
          alert(msg)
        });
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity 
          onPress={() => this.calculateSum()}
          style={styles.button}>
          <Text style={styles.text}>Calculate Sum</Text>
        </TouchableOpacity>   
      </SafeAreaView>
    );
  } 
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    borderColor: 'black',
    borderWidth: 1
  },
  text: {
    padding: 5
  }
});

// export default App;

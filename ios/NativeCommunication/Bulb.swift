//
//  Bulb.swift
//  NativeCommunication
//
//  Created by Sample Admin on 08/10/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation

@objc(Bulb) class Bulb: NSObject {
//  @objc static var isOn = false
//
//  @objc func turnOn() {
//    Bulb.isOn = true
//    print("Bulb is on now.")
//  }
//
  @objc static func requiresMainQueueSetup() -> Bool {
    return true
  }
//
//  @objc func turnOff() {
//    Bulb.isOn = false
//    print("Bulb is off now.")
//  }
//
//  @objc func getStatus(_ callback: RCTResponseSenderBlock) {
//    callback([NSNull(), Bulb.isOn])
//  }
  
  @objc func getSum(_ callback: RCTResponseSenderBlock) {  
    var sum = 0
    
    for i in 0..<1000 {
      for j in 0..<1000 {
        for k in 0..<1000 {
          sum = sum + i + j + k
        }
      }
    }
    callback([NSNull(), sum])
  }
}


//
//  Bulb.m
//  NativeCommunication
//
//  Created by Sample Admin on 08/10/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "React/RCTBridgeModule.h"

@interface RCT_EXTERN_MODULE(Bulb, NSObject)
//           RCT_EXTERN_METHOD(turnOn)
//           RCT_EXTERN_METHOD(turnOff)
//           RCT_EXTERN_METHOD(getStatus: (RCTResponseSenderBlock)callback)
           RCT_EXTERN_METHOD(getSum: (RCTResponseSenderBlock)callback)
@end

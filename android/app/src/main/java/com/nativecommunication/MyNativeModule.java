package com.nativecommunication;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;

public class MyNativeModule extends ReactContextBaseJavaModule {
    public MyNativeModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "Native";
    }

    @ReactMethod
    public void sayHi(Callback errorCallBack, Callback successCallback) {
        double sum = 0.0;
        for (int i = 0; i < 1000; i++) {
            for (int j = 0; j < 1000; j++) {
                for (int k = 0; k < 1000; k++) {
                    sum = sum + i + j + k;
                }
            }
        }
        try {
            System.out.println("Greetings From Java " + sum);
            successCallback.invoke("Loop length 1000,\nSum = " + sum);
        } catch (IllegalViewOperationException e) {
            errorCallBack.invoke(e.getMessage());
        }
    }
}
